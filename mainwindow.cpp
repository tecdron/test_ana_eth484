#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    sIPRelay= "192.168.1.14";
    iNumPort = 17494;

    qTcpRelay = new QTcpSocket();

    qTcpRelay->connectToHost(sIPRelay,iNumPort);

    tmrTimer= new QTimer();

    connect(tmrTimer,SIGNAL(timeout()),this,SLOT(processCmdAna()));

    tmrTimer->start(50);


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::processCmdAna()
{
    if (qTcpRelay->isOpen())
    {
        for (int i=1; i<5; i++)
        {

        SerBuf[0] = VAR_GET_INPUT;
        SerBuf[1] = (char)(i);
        transmit(2);
        receive(2);

        unsigned char tabTestConv[2];
        tabTestConv[0]= (unsigned char)SerBuf[0];
        tabTestConv[1]= (unsigned char)SerBuf[1];

        int iTest = (int)(tabTestConv[0] << 8)+ (int)(tabTestConv[1]);
        //ui->statusBar->showMessage("Réponse ETH484:" + QString::number(iTest));

        switch (i)
        {
            case 1:ui->progressBar->setValue(iTest);
                break;

            case 2:ui->progressBar_2->setValue(iTest);
                break;

            case 3:ui->progressBar_3->setValue(iTest);
                break;

            case 4:ui->progressBar_4->setValue(iTest);
                break;
         }


        }
    }
}

void MainWindow::transmit(int iLenght)
{
    qTcpRelay->write(SerBuf,iLenght);
    qTcpRelay->flush();
}

void MainWindow::receive(int iLenght)
{
    qTcpRelay->read(SerBuf,iLenght);

}
