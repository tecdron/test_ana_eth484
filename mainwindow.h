#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtNetwork/QTcpSocket>
#include <QtCore>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QString sIPRelay;
    int iNumPort;
    QTcpSocket* qTcpRelay;
    QTimer* tmrTimer;

    int NUMBER_OF_ADC_CHANNELS = 4;
    int NUMBER_OF_RELAYS = 4;

     char* SerBuf = new  char[70];

public slots:
    void processCmdAna();
    void transmit(int iLenght);
    void receive(int iLenght);

private:
    Ui::MainWindow *ui;
};

enum commands_eth484 : char
{
    GET_VER = 0x10,
    DIG_ACTIVE = 0x20, DIG_INACTIVE, DIG_DIRECTION, DIG_SET_OUTPUTS, DIG_GET_OUTPUTS, DIG_GET_INPUTS,
    VAR_SET_OUTPUT = 0x30, VAR_GET_OUTPUT, VAR_GET_INPUT,
    GET_SER_NUM = 0x77, GET_VOLTS, PASSWORD_ENTRY, GET_UNLOCK_TIME, LOG_OUT,
};

#endif // MAINWINDOW_H
